var FCM = require('fcm-push');
var dbOperations = require('./dbOperations.js');

const serverKey = 'AIzaSyDIqw9U66PW2pPyPhin3-CJ44jhEnpHpAg';
const fcm = new FCM(serverKey);

module.exports = {
  sendPushViaMulticast: function(fcmIds, payload) {
    return fcm.send({ registration_ids: fcmIds, data : payload.message }).then(res => res);
  }
};
