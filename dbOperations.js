module.exports = {
  getRecords: function(req, res) {
    var pg = require('pg');
    //You can run command "heroku config" to see what is Database URL from Heroku belt

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);
    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query('select * from users', (err, response) => {
          if (err) throw err;
          console.log(response);
          client.end();
          res.send({
            status: 200,
            data: response.rows
          });
        });
      })
      .catch(e => console.log('connection error', e));
  },

  sendAlert: function(req, res) {
    var pg = require('pg');
    var fcm = require('./fcm.js');
    //You can run command "heroku config" to see what is Database URL from Heroku belt

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);
    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query('select * from users', (err, response) => {
          if (err) throw err;
          console.log(response);
          client.end();
          const users = response.rows;
          var fcmIds = users
            .filter(
              user =>
                user.username !== req.query.username &&
                user.fcm &&
                user.fcm.length > 0
            )
            .map(user => user.fcm);
          var cu = users.filter(user => user.username === req.query.username);
          var payload = {
            message: {
              title: 'Safety Alert',
              message: `${
                cu[0].username.toUpperCase()
              } is in trouble. Please pay attention @ location ${
                cu[0].location
              } (${cu[0].lat},${cu[0].lng})`,
              username: cu[0].username.toUpperCase(),
              lat: cu[0].lat,
              lng: cu[0].lng,
              location: cu[0].locattion
            }
          };

          if (fcmIds.length > 0) {
            fcm
              .sendPushViaMulticast(fcmIds, payload)
              .then(() => res.send({ status: 200, data: 'success' }));
          } else {
            res.send({
              status: 206,
              data: 'Currently no registered users available!'
            });
          }
        });
      })
      .catch(e => console.log('connection error', e));
  },

  addRecord: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query(
          'insert into users (username,fcm,lat,lng,location) ' +
            "values ('" +
            req.query.username +
            "','" +
            req.query.fcm +
            "','" +
            req.query.lat +
            "','" +
            req.query.lng +
            "','" +
            req.query.location +
            "')",
          (err, response) => {
            if (err) throw err;
            console.log(response);
            res.send('Success');
            client.end();
          }
        );
      })
      .catch(e => console.log('connection error', e));
  },

  delRecord: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query(
          'Delete from users Where id =' + req.query.id,
          (err, response) => {
            if (err) throw err;
            console.log(response);
            res.send('Success');
            client.end();
          }
        );
      })
      .catch(e => console.log('connection error', e));
  },

  delAllUsers: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query('Delete from users ', (err, response) => {
          if (err) throw err;
          console.log(response);
          res.send('Success');
          client.end();
        });
      })
      .catch(e => console.log('connection error', e));
  },

  createUser: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query(
          "SELECT * FROM users WHERE username = '" + req.query.username + "'",
          (err, response) => {
            if (err) Promise.reject(err);
            console.log(response);
            if (response && response.rows && response.rows.length > 0) {
              res.send({
                status: 206,
                data: 'User already exists. Register with another username'
              });
              client.end();
            } else {
              client.query(
                'insert into users (username) ' +
                  "values ('" +
                  req.query.username +
                  "')",
                (err, response) => {
                  if (err) Promise.reject(err);
                  console.log(response);
                  res.send({ status: 200, data: 'Success' });
                  client.end();
                }
              );
            }
          }
        );
      })
      .catch(e => console.log('connection error', e));
  },

  createTable: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query(
          'CREATE TABLE users' +
            '(' +
            'username character varying(50),' +
            'fcm TEXT,' +
            'lat character varying(20),' +
            'lng character varying(20),' +
            'location TEXT,' +
            'id serial NOT NULL' +
            ')',
          (err, response) => {
            if (err) throw err;
            console.log(response);
            res.status(200);
            res.send('Table created');
            client.end();
          }
        );
      })
      .catch(e => console.log('connection error', e));
  },

  updateFcm: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query(
          "UPDATE users set fcm = '" +
            req.query.fcm +
            "' WHERE username = '" +
            req.query.username +
            "'",
          (err, response) => {
            if (err) throw err;
            console.log(response);
            res.send({ status: 200, data: 'Updated Successfully' });
            client.end();
          }
        );
      })
      .catch(e => console.log('connection error', e));
  },

  updateLocation: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query(
          "UPDATE users set lat = '" +
            req.query.lat +
            "'" +
            " , lng = '" +
            req.query.lng +
            "'" +
            " , location = '" +
            req.query.location +
            "'" +
            "WHERE username = '" +
            req.query.username +
            "'",
          (err, response) => {
            if (err) throw err;
            console.log(response);
            res.send({ status: 200, data: 'Updated Successfully' });
            client.end();
          }
        );
      })
      .catch(e => console.log('connection error', e));
  },

  dropTable: function(req, res) {
    var pg = require('pg');

    var conString =
      process.env.DATABASE_URL ||
      'postgres://jrwvmahpwodunt:7e1eb402cc12c6dc563ae419896ce697a200c6d9501d8de5afeadb2e67eeccdd@ec2-23-21-121-220.compute-1.amazonaws.com:5432/ddsijo438pp23s';
    var client = new pg.Client(conString);

    client
      .connect()
      .then(() => {
        console.log('connected');
        client.query('Drop TABLE ' + req.query.table, (err, response) => {
          if (err) throw err;
          console.log(response);
          res.send('Table droped');
          client.end();
        });
      })
      .catch(e => console.log('connection error', e));
  }
};
